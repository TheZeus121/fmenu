;;; package.scm --- build script for GNU Guix
;;; Copyright © 2020 Uko Koknevics <perkontevs@gmail.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; Fmenu is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Fmenu is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with fmenu.  If not, see <https://www.gnu.org/licenses/>.

(use-modules (gnu packages terminals)
             (gnu packages xdisorg)
             (guix build-system cmake)
             (guix gexp)
             ((guix licenses) #:prefix license:)
             (guix packages))

(package
  (name "fmenu")
  (version "0.1.0.0")
  (source (local-file (dirname (current-filename))
                      #:recursive? #t))
  (build-system cmake-build-system)
  (propagated-inputs
   `(("go-github-com-junegunn-fzf" ,go-github-com-junegunn-fzf)
     ("rxvt-unicode" ,rxvt-unicode)
     ("xdotool" ,xdotool)))
  (arguments
   `(#:tests? #f))
  (synopsis "Dynamic menu")
  (description
   "A dynamic menu for X, based on the idea of dmenu, but using fzf
under the hood.")
  (home-page "https://hub.darcs.net/Ukko/fmenu")
  (license license:gpl3+))
