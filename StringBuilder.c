/* StringBuilder.c --- Implementation of StringBuilder.h.
 * Copyright © 2020 Uko Koknevics <perkontevs@gmail.com>
 *
 * Fmenu is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fmenu is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with fmenu.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "StringBuilder.h"

#include <stdlib.h>
#include <string.h>

#define START_CAPACITY 256

struct StringBuilder {
	char *buf;
	size_t size;
	size_t capacity;
};

static void reserve(StringBuilder *sb, size_t newcap);

void sb_append_char(StringBuilder *sb, char chr) {
	if (!sb) {
		return;
	}

	if (sb->size == sb->capacity) {
		reserve(sb, sb->capacity * 3 / 2 + 1);
	}

	if (!sb->buf) {
		return;
	}

	sb->buf[sb->size++] = chr;
}

void sb_append_str(StringBuilder *sb, const char *str) {
	if (!sb) {
		return;
	}

	size_t newsize = strlen(str) + sb->size;

	if (newsize > sb->capacity) {
		reserve(sb, newsize * 3 / 2 + 1);
	}

	if (!sb->buf) {
		return;
	}

	strcpy(sb->buf + sb->size, str);
	sb->size = newsize;
}

const char *sb_build(StringBuilder *sb) {
	if (!sb) {
		return NULL;
	}

	if (sb->capacity == sb->size) {
		reserve(sb, sb->capacity * 3 / 2 + 1);
	}

	if (!sb->buf) {
		return NULL;
	}

	sb->buf[sb->size] = '\0';
	return sb->buf;
}

void sb_free(StringBuilder *sb) {
	if (!sb) {
		return;
	}

	if (!sb->buf) {
		free(sb);
		return;
	}

	free(sb->buf);
	free(sb);
}

StringBuilder *sb_new() {
	StringBuilder *sb = calloc(1, sizeof(StringBuilder));
	if (!sb) {
		return NULL;
	}

	sb->buf = calloc(START_CAPACITY, sizeof(char));
	if (!sb->buf) {
		free(sb);
		return NULL;
	}

	sb->capacity = START_CAPACITY;
	/* sb->size is already zero thanks to calloc. */

	return sb;
}

void sb_reset(StringBuilder *sb) {
	if (!sb) {
		return;
	}

	sb->size = 0;
}

static void reserve(StringBuilder *sb, size_t newcap) {
	if (sb->capacity >= newcap) {
		return;
	}

	sb->buf = realloc(sb->buf, newcap * sizeof(char));
	sb->capacity = newcap;
}
