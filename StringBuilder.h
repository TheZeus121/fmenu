/* StringBuilder.h --- Dynamically created strings.
 * Copyright © 2020 Uko Koknevics <perkontevs@gmail.com>
 *
 * Fmenu is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fmenu is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with fmenu.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#ifndef STRING_BUILDER_H_
#define STRING_BUILDER_H_

typedef struct StringBuilder StringBuilder;

/*! Creates a new StringBuilder.  Pointer should be freed with
 *  sb_free(). */
StringBuilder *sb_new();

/*! Frees the memory used by a StringBuilder.  The pointer should not
 *  be used again. */
void sb_free(StringBuilder *sb);

/*! Appends a string to the StringBuilder. */
void sb_append_str(StringBuilder *sb, const char *str);

/*! Appends a character to the StringBuilder. */
void sb_append_char(StringBuilder *sb, char chr);

/*! Gets the built string from the StringBuilder. */
const char *sb_build(StringBuilder *sb);

/*! Resets the StringBuilder. */
void sb_reset(StringBuilder *sb);

#endif  /* STRING_BUILDER_H_ */
