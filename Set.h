/* Set.h --- A finite set.
 * Copyright © 2020 Uko Koknevics <perkontevs@gmail.com>
 *
 * Fmenu is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fmenu is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with fmenu.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#ifndef SET_H_
#define SET_H_

#include <stdbool.h>

typedef struct Set Set;

/*! Creates a new set.  Pointer should be freed with set_free(). */
Set *set_new(void);

/*! Frees the memory used by a set.  The pointer should not be used
 *  again. */
void set_free(Set *set);

/*! Returns true if the set contains the specified element. */
bool set_contains(const Set *set, const char *elem);

/*! Adds (by copying) the specified element.  Returns true if the set
 *  didn't contain the specified element before. */
bool set_add(Set *set, const char *elem);

#endif  /* VECTOR_H_ */
