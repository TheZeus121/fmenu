/* Launch.c --- Implementation of Launch.h
 * Copyright © 2020 Uko Koknevics <perkontevs@gmail.com>
 *
 * Fmenu is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fmenu is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with fmenu.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Launch.h"

#include <stdio.h>
#include <unistd.h>

int launch(const char *name) {
	pid_t pid = fork();

	if (pid == 0) {
		/* Child */
		if (setsid() < 0) {
			perror("setsid");
		}

		execlp(name, name, NULL);
		perror("execlp");
		return -1;
	} else if (pid > 0) {
		/* Parent */
		return 0;
	} else {
		perror("fork");
		return -1;
	}
}
