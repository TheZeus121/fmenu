/* Set.c --- Implementation of Set.h
 * Copyright © 2020 Uko Koknevics <perkontevs@gmail.com>
 *
 * Fmenu is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fmenu is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with fmenu.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Set.h"

#include <stdlib.h>
#include <string.h>

#include "Hash.h"

#define HASH_SIZE 9
#define START_CAP 10

struct Bucket {
	char **data;
	size_t capacity;
	size_t size;
};

typedef struct Bucket Bucket;

struct Set {
	Bucket buckets[1 << HASH_SIZE];
};

static int strcmp_(const void *a, const void *b);
static bool bucket_add(Bucket *bucket, const char *elem);
static bool bucket_contains(const Bucket *bucket, const char *elem);

bool set_add(Set *set, const char *elem) {
	if (!set) {
		return false;
	}

	if (set_contains(set, elem)) {
		return false;
	}

	uint32_t h = hash(elem) & ((1 << HASH_SIZE) - 1);

	return bucket_add(&set->buckets[h], elem);
}

bool set_contains(const Set *set, const char *elem) {
	if (!set) {
		return false;
	}

	uint32_t h = hash(elem) & ((1 << HASH_SIZE) - 1);

	return bucket_contains(&set->buckets[h], elem);
}

void set_free(Set *set) {
	if (!set) {
		return;
	}

	for (size_t i = 0; i < 1 << HASH_SIZE; i++) {
		if (set->buckets[i].data) {
			free(set->buckets[i].data);
		}
	}

	free(set);
}

Set *set_new(void) {
	return calloc(1, sizeof(Set));
}

static bool bucket_add(Bucket *bucket, char const *elem) {
	if (!bucket) {
		return false;
	}

	if (bucket_contains(bucket, elem)) {
		return false;
	}

	char *new_elem = calloc(strlen(elem) + 1, sizeof(char));
	strcpy(new_elem, elem);

	if (bucket->size == bucket->capacity) {
		size_t new_cap = bucket->capacity * 3 / 2 + 1;
		bucket->data = realloc(bucket->data, new_cap * sizeof(char *));
		bucket->capacity = new_cap;
	}

	if (!bucket->data) {
		return false;
	}

	bucket->data[bucket->size++] = new_elem;
	qsort(bucket->data, bucket->size, sizeof(char *), strcmp_);

	return true;
}

static bool bucket_contains(const Bucket *bucket, const char *elem) {
	if (!bucket || !bucket->data) {
		return false;
	}

	if (bsearch(&elem,
	            bucket->data,
	            bucket->size,
	            sizeof(char *),
	            strcmp_)) {
		return true;
	} else {
		return false;
	}
}

static int strcmp_(const void *a, const void *b) {
	return strcmp(*((const char **) a),
	              *((const char **) b));
}
