/* main.c --- Entry point.
 * Copyright © 2020 Uko Koknevics <perkontevs@gmail.com>
 *
 * Fmenu is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fmenu is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with fmenu.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

#include "Launch.h"
#include "Set.h"
#include "StringBuilder.h"

static size_t count_paths(const char *path);
/** Must free() the returned pointer. */
static char *get_path(void);
static bool is_executable(struct stat statv);
static void process_pathdir(const char *dir,
                            FILE *out,
                            Set *set,
                            StringBuilder *sb);
static void read_line(FILE *f, StringBuilder *sb);
/** Must free() the returned pointer. */
static char **split_path(char *path);
static int strcmp_(const void *a, const void *b);

int main(void) {
	int stdin_pipe[2];
	int stdout_pipe[2];
	pid_t child;

	if (pipe(stdin_pipe)) {
		perror("pipe");
		return 1;
	}

	if (pipe(stdout_pipe)) {
		perror("pipe");
		return 1;
	}

	child = fork();
	if (child == 0) {
		/* This is child */
		close(stdin_pipe[1]);
		close(stdout_pipe[0]);

		if (dup2(stdin_pipe[0], STDIN_FILENO) < 0) {
			perror("dup2");
			return 1;
		}

		if (dup2(stdout_pipe[1], STDOUT_FILENO) < 0) {
			perror("dup2");
			return 1;
		}

		return execlp("fzf", "fzf",
		              "--prompt=run> ",
		              "--color=16",
		              NULL);
	} else if (child > 0) {
		/* This is parent */
		FILE *fzf_input, *fzf_output;

		close(stdin_pipe[0]);
		close(stdout_pipe[1]);

		fzf_input = fdopen(stdin_pipe[1], "w");
		fzf_output = fdopen(stdout_pipe[0], "r");
		if (!fzf_input || !fzf_output) {
			perror("fdopen");
			return 1;
		}

		char *path_env;
		path_env = get_path();

		char **paths = split_path(path_env);

		Set *set = set_new();

		set_add(set, ".");
		set_add(set, "..");

		StringBuilder *sb = sb_new();

		for (size_t i = 0; paths[i]; i++) {
			process_pathdir(paths[i], fzf_input, set, sb);
		}

		fclose(fzf_input);
		set_free(set);
		free(paths);
		free(path_env);
		wait(NULL);

		read_line(fzf_output, sb);
		fclose(fzf_output);

		int ret = launch(sb_build(sb));
		sb_free(sb);
		return ret;
	} else {
		/* Error */
		perror("fork");
		return 1;
	}
}

size_t count_paths(const char *path) {
	size_t ret = 0;

	for (size_t i = 0; path[i] != '\0'; i++) {
		if (path[i] == ':') {
			ret++;
		}
	}

	return ret + 1;
}

char *get_path(void) {
	const char *env_path;
	char *path;

	env_path = getenv("PATH");
	if (env_path == NULL) {
		return NULL;
	}

	path = calloc(1 + strlen(env_path), sizeof(char));
	return strcpy(path, env_path);
}

bool is_executable(struct stat statv) {
	if (!S_ISREG(statv.st_mode)) {
		return false;
	}

	if (statv.st_mode & S_IXOTH) {
		return true;
	}

	if (statv.st_gid == getegid() && (statv.st_mode & S_IXGRP)) {
		return true;
	}

	if (statv.st_uid == geteuid() && (statv.st_mode & S_IXUSR)) {
		return true;
	}

	return false;
}

void process_pathdir(const char *dirname,
                     FILE *out,
                     Set *set,
                     StringBuilder *sb) {
	struct stat statv;

	if (stat(dirname, &statv)) {
		return;
	}

	if (!S_ISDIR(statv.st_mode)) {
		return;
	}

	DIR *dir = opendir(dirname);
	if (!dir) {
		perror("opendir");
	}

	for (struct dirent *entry = readdir(dir);
	     entry;
	     entry = readdir(dir)) {
		sb_append_str(sb, dirname);
		sb_append_char(sb, '/');
		sb_append_str(sb, entry->d_name);

		const char *fname = sb_build(sb);
		sb_reset(sb);

		if (stat(fname, &statv)) {
			continue;
		}

		if (!is_executable(statv)) {
			continue;
		}

		if (set_add(set, entry->d_name)) {
			fprintf(out, "%s\n", entry->d_name);
		}
	}
}

void read_line(FILE *f, StringBuilder *sb) {
	int c = fgetc(f);

	while (c != EOF && c != '\n') {
		sb_append_char(sb, (char) c);
		c = fgetc(f);
	}
}

char **split_path(char *path) {
	size_t count = count_paths(path);
	char **ret = calloc(count, sizeof(char *));
	size_t i = 0;

	/* Split the path into tokens. */
	for (char *tok = strtok(path, ":");
	     tok != NULL;
	     tok = strtok(NULL, ":")) {
		ret[i++] = tok;
	}

	/* Sort the path array. */
	qsort(ret, count, sizeof(char *), strcmp_);

	/* Eliminate non-uniques. */
	i = 1;
	size_t free_loc = 1;
	for (; i < count; i++) {
		if (strcmp(ret[i], ret[i - 1])) {
			/* Current string is unique. */
			if (i != free_loc) {
				ret[free_loc] = ret[i];
			}

			free_loc++;
		}
	}

	for (; free_loc < count; free_loc++) {
		ret[free_loc] = NULL;
	}

	return ret;
}

int strcmp_(const void *a, const void *b) {
	return strcmp(*((const char **) a), *((const char **) b));
}
