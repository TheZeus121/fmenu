/* Launch.h --- Launching executables.
 * Copyright © 2020 Uko Koknevics <perkontevs@gmail.com>
 *
 * Fmenu is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fmenu is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with fmenu.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#ifndef LAUNCH_H_
#define LAUNCH_H_

/* Returns non-zero if stuff fails. */
int launch(const char *name);

#endif  /* LAUNCH_H_ */
